package net.kamradtfamily.syncservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/contrived")
public class SyncController {
    private final Messaging messaging;
    private final ObjectMapper objectMapper;
    public SyncController(Messaging messaging,
                   ObjectMapper objectMapper) {
        this.messaging = messaging;
        this.objectMapper = objectMapper;
    }
    @GetMapping(path="/example/{message}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    Mono<ResponseEntity<JsonNode>> initiateTransaction(
            @PathVariable("message") final String message) {
        return Mono.just(objectMapper
                    .createObjectNode()
                    .put("message", message))
            .flatMap(messaging::addToQueueAndWaitForResponse)
            .map(json-> ResponseEntity.ok().body(json))
            .onErrorResume(e -> Mono
                .just(ResponseEntity.badRequest().
                   body(objectMapper.createObjectNode()
                       .put("failures", e.getMessage()))));


    }
}
