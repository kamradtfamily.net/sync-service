package net.kamradtfamily.syncservice;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RMapCacheReactive;
import org.redisson.api.RedissonReactiveClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class SyncServiceDistributedMap {
    private final RMapCacheReactive<String, JsonNode> responseMap;
    public SyncServiceDistributedMap(@Value("${redisson.address}") String address,
                                     @Value("${redisson.password}") String password) {
        try {
            Config redissonConfig = new Config();
            redissonConfig
                    .useSingleServer()
                    .setAddress(address)
                    .setPassword(password);
            RedissonReactiveClient redisson =
                    Redisson.create(redissonConfig).reactive();
            responseMap = redisson.getMapCache("sync-service");
        } catch(Exception ex) {
            log.error("exception creating redisson client", ex);
            throw ex; // spring doesn't display a good error message
        }
    }

    public Mono<Boolean> put(String response, JsonNode json) {
        return responseMap.fastPut(response, json, 10,
                TimeUnit.MINUTES);
    }

    public Mono<JsonNode> get(String message) {
        return responseMap.get(message);
    }

    public Mono<Boolean> containsKey(String message) {
        return responseMap.containsKey(message);
    }
}
