package net.kamradtfamily.syncservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.Properties;

@SpringBootApplication
@Slf4j
public class SyncServiceApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(SyncServiceApplication.class, args);
		Properties git = new Properties();
		git.load(SyncServiceApplication.class.getResourceAsStream("/git.properties"));
		git.entrySet().stream().forEach(es -> log.info("{}: {}", es.getKey(), es.getValue()));
	}

	@Bean
	ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

}
