package net.kamradtfamily.syncservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;
import reactor.retry.Repeat;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Configuration
@Slf4j
public class Messaging {
    private final BlockingQueue<JsonNode> queue =
            new SynchronousQueue<>();
    private final ObjectMapper objectMapper;
    private final SyncServiceDistributedMap syncServiceDistributedMap;

    public Messaging(ObjectMapper objectMapper,
                     SyncServiceDistributedMap syncServiceDistributedMap) {
        this.objectMapper = objectMapper;
        this.syncServiceDistributedMap = syncServiceDistributedMap;
    }

    public Mono<JsonNode> addToQueueAndWaitForResponse(JsonNode json) {
        queue.add(json);
        return syncServiceDistributedMap.containsKey(json.path("message").asText(""))
                .filter(b -> b)
                .repeatWhenEmpty(
                        Repeat.onlyIf(repeatContext -> true)
                                .exponentialBackoff(Duration.ofSeconds(1), Duration.ofSeconds(1))
                                .timeout(Duration.ofSeconds(30)))
                .switchIfEmpty(Mono.error(new IllegalStateException("could not find event")))
                .flatMap(b -> syncServiceDistributedMap.get(json
                                .path("message")
                                .asText("")));
    }

    @Bean
    public Supplier<String> eventProducer(){
        return () -> {
            try {
                JsonNode item = queue.take();
                log.info("pulled {} from queue", item);
                return item.toPrettyString();
            } catch (InterruptedException e) {
                log.warn("process interrupted");
                Thread.currentThread().interrupt();
                throw new IllegalStateException("process interrupted");

            }
        };
    }

    @Bean
    public Consumer<String> eventConsumer() {
        return value -> Mono.just(value)
                .doOnNext(s -> log.info("consumed {}", s))
                .flatMap(v -> {
                    try {
                        return Mono.just(objectMapper.readTree(v));
                    } catch (JsonProcessingException e) {
                        log.warn("exception deserializing {}", v);
                        return Mono.error(e);
                    }
                })
                .doOnNext(json -> log.info("message {} received", json))
                .flatMap(json -> syncServiceDistributedMap
                        .put(json
                                .path("response")
                                .asText(""),
                                json))
                .subscribe(i -> log.info("consume returned {}", i),
                        e -> log.warn("consume returned error", e));
    }


    private static class EmptyException extends Exception {
    }
}
