package net.kamradtfamily.syncservice;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;

/** 
* AsyncServiceApplication Tester. 
* 
* @author <Authors name> 
* @since <pre>Dec 25, 2022</pre> 
* @version 1.0 
*/
@ExtendWith(SpringExtension.class)
@WebFluxTest(SyncController.class)
@Slf4j
class SyncControllerTest {
    @Autowired
    WebTestClient webTestClient;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    Messaging messaging;

    //@Test
    void stupidTest() throws IOException {
        SyncServiceApplication.main(new String[] {""});
    }


    @Test
    void testProcess() throws Exception {
        SyncController sut = new SyncController(messaging, objectMapper);
        JsonNode json = objectMapper.createObjectNode()
                .put("message", "response1");
        Mockito.when(messaging.addToQueueAndWaitForResponse(any(JsonNode.class))).thenReturn(Mono.just(json));

        webTestClient.get().uri("/contrived/example/message1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(JsonNode.class)
                .isEqualTo(json)
                .value(j -> log.info("found {}", j));

        Mono<ResponseEntity<JsonNode>> mono = sut.initiateTransaction("message1");
    }

    @Test
    void testError() throws Exception {
        SyncController sut = new SyncController(messaging, objectMapper);
        JsonNode json = objectMapper.createObjectNode()
                .put("message", "response1");
        Mockito.when(messaging.addToQueueAndWaitForResponse(any(JsonNode.class))).thenReturn(Mono.error(() -> new IllegalStateException("this is the error")));

        webTestClient.get().uri("/contrived/example/message1")
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody(JsonNode.class)
                .isEqualTo(objectMapper.createObjectNode()
                        .put("failures", "this is the error"))
                .value(j -> log.info("found {}", j));

        Mono<ResponseEntity<JsonNode>> mono = sut.initiateTransaction("message1");
    }

}