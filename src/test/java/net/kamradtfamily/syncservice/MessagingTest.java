package net.kamradtfamily.syncservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

/** 
* AsyncServiceApplication Tester. 
* 
* @author <Authors name> 
* @since <pre>Dec 25, 2022</pre> 
* @version 1.0 
*/
@Slf4j
class MessagingTest {

    ObjectMapper objectMapper = new ObjectMapper();

    Messaging sut;

    @Mock
    SyncServiceDistributedMap syncServiceDistributedMap;

    Map<String, JsonNode> map = new HashMap<>();

    @BeforeEach
    void beforeEach() {
        MockitoAnnotations.openMocks(this);
        Mockito.when(syncServiceDistributedMap.put(any(String.class), any(JsonNode.class)))
                .thenAnswer(invocationOnMock -> Mono.just(invocationOnMock)
                        .doOnNext(i -> map.put(i.getArgument(0), i.getArgument(1)))
                        .thenReturn(true));

        Mockito.when(syncServiceDistributedMap.containsKey(any(String.class)))
                .thenAnswer(invocationOnMock -> Mono.just(invocationOnMock)
                        .map(i -> map.containsKey((String)i.getArguments()[0])));
        Mockito.when(syncServiceDistributedMap.get(any(String.class)))
                .thenAnswer(invocationOnMock -> Mono.just(invocationOnMock)
                        .map(i -> map.get((String)invocationOnMock.getArguments()[0])));
        sut = new Messaging(objectMapper, syncServiceDistributedMap);

    }


    @Test
    void testProcess() throws Exception {
        log.info("in testProcess");
        String keyvalue = "Key1";
        Disposable kafka = createFunctionalKafkaMock(sut.eventProducer(), sut.eventConsumer());
        log.info("mockafka set up");
        Thread.sleep(100);
        JsonNode json = objectMapper.createObjectNode()
                        .put("message", keyvalue);
        log.info("adding to queue {}", json);
        Mono<JsonNode> mono = sut.addToQueueAndWaitForResponse(json);
        log.info("waiting for response");
        StepVerifier.create(mono)
                .assertNext(j -> assertEquals(keyvalue, j.path("response").asText("")))
                .verifyComplete();
        log.info("response found, closing mockafka");
        kafka.dispose();
    }

    @Test
    void testProcessBadResponse() throws Exception {
        log.info("in testProcessBadResponse");
        String keyvalue = "Key1";
        Disposable kafka = createBadFunctionalKafkaMock(sut.eventProducer(), sut.eventConsumer());
        log.info("mockafka set up");
        Thread.sleep(100);
        JsonNode json = objectMapper.createObjectNode()
                .put("message", keyvalue);
        log.info("adding to queue {}", json);
        Mono<JsonNode> mono = sut.addToQueueAndWaitForResponse(json);
        log.info("waiting for response");
        StepVerifier.create(mono)
                .expectErrorMatches(es -> {
                    return es instanceof IllegalStateException
                            && "could not find event".equals(es.getMessage());
                } );
        log.info("response found, closing mockafka");
        kafka.dispose();
    }

    private Disposable createFunctionalKafkaMock(Supplier<String> supplier, Consumer<String> consumer) {
        return Flux.from(Mono.fromSupplier(supplier))
                .doOnNext(s -> log.info("supplying for producer {}", s))
                .flatMap(s -> {
                    try {
                        return Mono.just(objectMapper.readTree(s));
                    } catch (JsonProcessingException e) {
                        return Mono.error(e);
                    }
                })
                .map(json -> objectMapper.createObjectNode()
                        .put("response", json.path("message").asText("")))
                .subscribeOn(Schedulers.single())
                .subscribe(s -> consumer.accept(s.toPrettyString()));

    }

    private Disposable createBadFunctionalKafkaMock(Supplier<String> supplier, Consumer<String> consumer) {
        return Flux.from(Mono.fromSupplier(supplier))
                .doOnNext(s -> log.info("supplying for producer {}", s))
                .flatMap(s -> {
                    try {
                        return Mono.just(objectMapper.readTree(s));
                    } catch (JsonProcessingException e) {
                        return Mono.error(e);
                    }
                })
                .map(json -> objectMapper.createObjectNode()
                        .put("response", "some other value"))
                .subscribeOn(Schedulers.single())
                .subscribe(s -> consumer.accept(s.toPrettyString()));

    }

}